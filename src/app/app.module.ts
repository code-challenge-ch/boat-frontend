import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { BoatDetailComponent } from './components/boat/boat-detail/boat-detail.component';
import { BoatEditComponent } from './components/boat/boat-edit/boat-edit.component';
import { BoatAddComponent } from './components/boat/boat-add/boat-add.component';
import { BoatOverviewComponent } from './components/boat/boat-overview/boat-overview.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/error/not-found/not-found.component';
import { UnauthorizedComponent } from './components/error/unauthorized/unauthorized.component';
import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    BoatDetailComponent,
    BoatEditComponent,
    BoatAddComponent,
    BoatOverviewComponent,
    DashboardComponent,
    NotFoundComponent,
    UnauthorizedComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
