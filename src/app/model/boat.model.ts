import { User } from './user.model';
export class Boat {
    id?: number;
    price?: number;
    name?: string ="";
    description?: string = "";
    model?: string ="";
    location?: string ="";
    user?: User;
}