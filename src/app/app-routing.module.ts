import { BoatAddComponent } from './components/boat/boat-add/boat-add.component';
import { BoatEditComponent } from './components/boat/boat-edit/boat-edit.component';
import { BoatDetailComponent } from './components/boat/boat-detail/boat-detail.component';
import { BoatOverviewComponent } from './components/boat/boat-overview/boat-overview.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UnauthorizedComponent } from './components/error/unauthorized/unauthorized.component';
import { NotFoundComponent } from './components/error/not-found/not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '404', component: NotFoundComponent },
  { path: '401', component: UnauthorizedComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'boats', component: BoatOverviewComponent, canActivate: [AuthGuard] },
  { path: 'boat/detail/:id', component: BoatDetailComponent, canActivate: [AuthGuard] },
  { path: 'boat/edit/:id', component: BoatEditComponent, canActivate: [AuthGuard] },
  { path: 'boat/add', component: BoatAddComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
