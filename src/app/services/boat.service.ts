import { User } from './../model/user.model';
import { Observable } from 'rxjs';
import { Boat } from './../model/boat.model';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';

const API_URL = "http://localhost:8080/api";

@Injectable({
  providedIn: 'root'
})
export class BoatService {

  currentUser: User;
  headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.headers = new HttpHeaders({
      authorization:'Bearer ' + this.currentUser.token,
      "Content-Type":"application/json; charset=UTF-8"
    });
  }
  
  getAllBoats(): Observable<Array<Boat>> {
    return this.http.get<Array<Boat>>(API_URL + '/boats/', {headers: this.headers});
  }

  addBoat(boat: any): Observable<any> {
    return this.http.post(API_URL + '/boat/', boat, {headers: this.headers});
  }

  updateBoat(boat: any): Observable<any> {
    return this.http.put(API_URL + '/boat/' + boat.id, boat, {headers: this.headers});
  }

  getBoat(id: string): Observable<Boat> {
    return this.http.get<Boat>(API_URL + '/boat/' + id, {headers: this.headers});
  }

  deleteBoat(id: string): Observable<Boat> {
    return this.http.delete<Boat>( API_URL + '/boat/' + id, {headers: this.headers});
  }
}
