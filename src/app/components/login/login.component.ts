import { AlertifyService } from './../../services/alertify.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(
    private userService: UserService, 
    private router: Router, 
    private alertifyService: AlertifyService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log("username: " + this.username);
    this.userService.login(this.username, this.password)
      .subscribe(res => {
        this.alertifyService.success('You are logged in');
        this.router.navigate(['/dashboard']);
      }, err => {
        this.alertifyService.error(err.message);
      });
  }

}
