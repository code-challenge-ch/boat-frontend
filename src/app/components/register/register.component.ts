import { User } from './../../model/user.model';
import { AlertifyService } from './../../services/alertify.service';
import { Router } from '@angular/router';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  username: string;
  password: string;

  user: User = {
    id: 0,
    username: "",
    password: "",
    name: "",
    token: "", 
  };

  constructor(
    private userService: UserService, 
    private router: Router, 
    private alertifyService: AlertifyService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.userService.register(this.user)
      .subscribe(res => {
        this.alertifyService.success('User has been created.');
        this.router.navigate(['/login']);
      }, err => {
        this.alertifyService.error(err.message);
      });
  }

}
