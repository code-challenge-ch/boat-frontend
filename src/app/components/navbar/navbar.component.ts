import { Router } from '@angular/router';
import { AlertifyService } from './../../services/alertify.service';
import { User } from './../../model/user.model';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: boolean;
  currentLoggedInUser: string;

  constructor(
    private userService: UserService,
    private alertifyService: AlertifyService,
    private router: Router) { }

  ngOnInit(): void {
   this.userService.currentUserSubject.subscribe( res => {
      if(res){
        this.isLoggedIn = true;
        this.currentLoggedInUser = res.username;
      } else {
        this.isLoggedIn = false;
      }
    });
      
  }

  logout(){
    this.userService.logOut().subscribe(res => {
      this.alertifyService.success('You are logged out');
        this.router.navigate(['/login']);
    }, err => {
      this.alertifyService.error(err.message);
    });
  }

}
