import { Boat } from './../../../model/boat.model';
import { Router, ActivatedRoute } from '@angular/router';
import { BoatService } from './../../../services/boat.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boat-edit',
  templateUrl: './boat-edit.component.html',
  styleUrls: ['./boat-edit.component.scss']
})
export class BoatEditComponent implements OnInit {

  id: string;
  boat: Boat = {
    name: '',
    description: '',
    price: 0,
    model: '',
    location :''
  };

  constructor(
    private boatService: BoatService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.boatService.getBoat(this.id).subscribe(x => this.boat = x);
  }

  onSubmit(form): void {
    form.value.id = this.id;
    this.boatService.updateBoat(this.boat).subscribe();
    this.router.navigate(['/boat/detail/' + this.id]);
  }

}
