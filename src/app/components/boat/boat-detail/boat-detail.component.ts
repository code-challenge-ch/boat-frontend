import { AlertifyService } from './../../../services/alertify.service';
import { Observable } from 'rxjs';
import { Boat } from './../../../model/boat.model';
import { Router, ActivatedRoute } from '@angular/router';
import { BoatService } from './../../../services/boat.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boat-detail',
  templateUrl: './boat-detail.component.html',
  styleUrls: ['./boat-detail.component.scss']
})
export class BoatDetailComponent implements OnInit {

  boat$: Observable<Boat>;
  id: string;

  constructor(
    private boatService: BoatService,
    private router: Router,
    private route: ActivatedRoute,
    private alertifyService: AlertifyService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.boat$ = this.boatService.getBoat(this.id);
  }

  onDelete(): void {
    if (confirm('Are you sure?')) {
      this.boatService.deleteBoat(this.id).subscribe();
      this.alertifyService.success('Boat deleted. Maybe take a few seconds.');
      this.router.navigate(['/']);
    }
  }

}
