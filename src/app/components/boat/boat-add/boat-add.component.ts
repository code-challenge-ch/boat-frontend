import { User } from './../../../model/user.model';
import { UserService } from './../../../services/user.service';
import { AlertifyService } from './../../../services/alertify.service';
import { Router } from '@angular/router';
import { BoatService } from './../../../services/boat.service';
import { Boat } from './../../../model/boat.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boat-add',
  templateUrl: './boat-add.component.html',
  styleUrls: ['./boat-add.component.scss']
})
export class BoatAddComponent implements OnInit {

  user: User;
  boat: Boat = {
    name: '',
    description: '',
    price: 0,
    model: '',
    location :''
  };

  constructor(
    private boatService: BoatService,
    private userService: UserService,
    private router: Router,
    private alertifyService: AlertifyService) { }

  ngOnInit(): void {
    this.userService.currentUser.subscribe(user => {
      this.user = user;
    });
  }

  onSubmit(form) {
    this.boat.user = this.user;
    this.boatService.addBoat(this.boat).subscribe(
      result => this.alertifyService.success('Boat created'),
      err => {
        this.alertifyService.error('Something went wrong: ' + err.message);
      }
  );     
    this.router.navigate(['/']);
  }

}
