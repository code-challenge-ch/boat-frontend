import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatOverviewComponent } from './boat-overview.component';

describe('BoatOverviewComponent', () => {
  let component: BoatOverviewComponent;
  let fixture: ComponentFixture<BoatOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoatOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
