import { AlertifyService } from './../../../services/alertify.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Boat } from './../../../model/boat.model';
import { BoatService } from './../../../services/boat.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boat-overview',
  templateUrl: './boat-overview.component.html',
  styleUrls: ['./boat-overview.component.scss']
})
export class BoatOverviewComponent implements OnInit {

  boats$: Observable<Boat[]>;

  constructor(
    private boatService: BoatService, 
    private router: Router,
    private alertifyService: AlertifyService) { }

  ngOnInit(): void {
     this.boats$ = this.boatService.getAllBoats();
    console.log(this.boats$.subscribe(x => console.log(x)));
  }

}
